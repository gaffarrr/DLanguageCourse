import React, { useState, component } from "react";
import { Grid, Box ,Link,List,Collapse,ListItemButton,ListItemText,Button} from "@mui/material";
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Header from '../header/Header'
import HeaderUser from "../headerUser/HeaderUser";
import Axios from 'axios';
import { useEffect} from 'react'
import {useParams} from "react-router-dom"
import http from '../Axios/Config'

const CourseClass = () => {
    let id=useParams()
    let student_id=2
    const [open,setOpen]=React.useState(false)
    const [buttonstate,setbuttonstate]=React.useState(false)
    const [currentschedule,setcurrentschedule]=useState("Select Schedule")
    const [course, setCourseName] = useState([])
    const [othercourses, setothercourses] = useState([])
    const [schedules,setSchedules]=useState([])
    const GetCourseName=(id)=>{
        http.get('course/detail/'+id).
            then((response)=>
            {
                if(response.status===200){
                    //console.log(response.data)
                    setCourseName(response.data)
                }
            }).catch((err)=>{
                console.log(err)
            })
    }
    const GetOtherCourseName=(langid,id)=>{
        http.get('course/'+langid+'/except/'+id).
            then((response)=>
            {
                if(response.status===200){
                    //console.log(response.data)
                    setothercourses(response.data)
                }
            }).catch((err)=>{
                console.log(err)
            })
    }
    const GetSchedules=(id)=>{
        http.get('schedule/'+id).
            then((response)=>
            {
                if(response.status===200){
                    console.log(response.data)
                    setToDate(response.data)
                    console.log(schedules.schedule)
                }
            }).catch((err)=>{
                console.log(err)
            })
        
    }
    const handleClick=()=>{
        setOpen(!open);
    }
    const setToDate=(date)=>{
        console.log(date)
        for(let i=0;i<date.length;i++){
            let x = ""
            x = x+date[i].schedule
            console.log(x)
            x=new Date(x)
            console.log(x)
            date[i].schedule=x
            console.log(date[i].schedule)
            AddSchedule(date[i])
        }
        
    }
    const AddSchedule=(date)=>{
        setSchedules(...schedules,date)
    }
    const toStringDate=(item)=>{
        let x = `${item.GetDay}, ${item.getDate} ${item.getMonth} ${item.getYear}`;
        return(x)
    }
    const postCart=(student_id,course_id,schedule)=>{
        http.post('cart/',{
            student_id:student_id,
            course_id:course_id,
            schedule:schedule
        }).
            then((response)=>
            {
                if(response.status===200){
                    console.log(response.data)
                }
            }).catch((err)=>{
                console.log(err)
            })
    }
    useEffect(()=>{
        //console.log(id.id)
        GetCourseName(id.id)
        GetOtherCourseName(id.langid,id.id)
        GetSchedules(id.id)
    },[])
    return (
        <div>
            <Header />
            <img src={"/images/thumbnail" + course.image_file}></img>
            <h6>{course.language_name}</h6>
            <h4>{course.course_name}</h4>
            <List>
                <ListItemButton onClick={handleClick}>
                    <ListItemText primary={currentschedule}></ListItemText>
                    {open? <ExpandLess/>:<ExpandMore/>}
                </ListItemButton>
                <Collapse in={open} timeout="auto" unmountOnExit>
                {
                    schedules.map((item, index) =>
                        <List item key={index} component="div" disablePadding>
                            <ListItemButton onClick={()=>{
                                handleClick();
                                setcurrentschedule(item.schedule)
                                setbuttonstate(true)
                            }}>
                                {toStringDate(item.schedule)}
                            </ListItemButton>
                        </List>
                    )
                }
                   
                </Collapse>
            </List>
            {buttonstate? 
                <>
                    <Button variant="outlined" href='/' onClick={() => {
                        postCart(student_id, course.id, currentschedule);
                    } }>Add to cart</Button>
                    <Button variant="outlined" href='/checkout' onClick={() => {
                        postCart(student_id, course.id, currentschedule);
                    } }>Buy now</Button>
                </>
                    :
                <>
                    <Button variant="outlined" disabled>Add to cart</Button>
                    <Button variant="outlined" disabled>Buy now</Button>
                </>
             }
            
            
            


            <h3>Description</h3>
            <p>{course.description}</p>
            <h5>Another class for you</h5>

            <Box container align="center" sx={{ display: 'grid', gridTemplateColumns: 'repeat(3,1fr)' }}>
                {
                    othercourses.map((item, index) =>
                        <Grid item key={index}>
                            <Link href={"/Languages/"+item.language_id+"/course/"+item.id}>
                                <img src={"/images/thumbnail" + item.image_file}></img>
                                <h6 align="left">{item.language_name}</h6>
                                <h5 align="left">{item.course_name}</h5>
                                <p align="left">IDR {item.price}</p>
                            </Link>
                        </Grid>
                    )
                }
            </Box>
        </div>

    );
}
export default CourseClass;
