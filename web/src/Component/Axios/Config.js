import Axios from 'axios'

const http = Axios.create({
    baseURL:"http://localhost:5000/api/",
    setTimeout: 60000
})


export default http
//baseURL:"http://localhost:5000/api/"
//baseURL: "https://talentadigital.id:2028/api/"