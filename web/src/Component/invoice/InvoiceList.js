import React, { useState, Component,useEffect} from 'react'
import { List, Box,Link } from "@mui/material";
import Axios from 'axios';
import {useParams} from "react-router-dom"
import Header from '../header/Header';
import Footer from '../footer/Footer'
import http from '../Axios/Config'

const InvoiceList = () => {
  const [invoices, setInvoices] = useState([])
  const GetInvoices=(id)=>{
    http.get("invoice/display/"+id).
      then((response)=>
      {
          if(response.status===200){
              console.log(response.data)
              setInvoices(response.data)
          }
      }).catch((err)=>{
          console.log(err)
      })
    }
    useEffect(()=>{
      GetInvoices(1)//must be user id for final
    },[])
  return (
    <div>
      <Header></Header>
      <h4 align="left">Home &gt; Invoice</h4>
      <h5 align="left">Menu Invoice</h5>
      {/* <>{if(typeof invoices !=='undefined' ){

      }}
      </> */}
      <Box container align="center" sx={{ display: 'list-item' }}>
        {
          invoices.map((item, index) =>
            <List item key={index}>{/**Link will be done after string management is done ***/}
              <img src={"images/thumbnail" + item.id}></img>
              <h6 align="left">{item.date}</h6>
              <h5 align="left">{item.total_course}</h5>
              <p align="left">SCHEDULE: {item.total_price}</p>
            </List>
          )
        }
      </Box>
      <Footer></Footer>
    </div>

  )
}


export default InvoiceList