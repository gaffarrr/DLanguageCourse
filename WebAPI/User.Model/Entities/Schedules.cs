﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.Globalization;
using System.Text.Json;

namespace DLanguage.Model.Entities
{
    public class Schedules
    {
        public int course_id { get; set; }
        public DateTime schedule { get; set; }
    }
}
